#!/usr/bin/python
# coding=utf-8

import logging
import argparse
import cv2
from common import *
from multiple_ssh_client import MultipleSSHClient

def configureArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--fileName', help="The file name of the video to process.")
    parser.add_argument('--outFolder', help="Folder for writing collected faces.",
                        default="/home/juan/cdp/faces/news")
    parser.add_argument('--haarFolder', help="Folder for writing collected faces.",
                        default="/home/juan/cdp/03_sinfonia_big_data/opencv/data/haarcascades")
    parser.add_argument('--outputWidth', help="Output with for images to display in windows.",
                        default="600", type=int)
    parser.add_argument('--sshHost', help='Remote host for writing collected faces.',
                        default='')
    parser.add_argument('--sshUser', help='Remote user for writing collected faces in remote host.',
                        default='')
    parser.add_argument('--sshPassword', help='Remote password for writing collected faces in remote host.',
                        default='')
    parser.add_argument('--remoteFolder', help='Remote folder for writing collected faces.',
                        default='/home/juan/')
    parser.add_argument('--log', help="Log level for logging.", default="INFO")
    return parser.parse_args()


def buildFilename(contador):
    return 'face_' + str(contador) + '.jpg'

def buildFilepath(filename, folder=""):
    return folder + '/' + filename

def buildVideoCapture(fileName):
    if fileName:
        return cv2.VideoCapture(fileName)
    else:
        return cv2.VideoCapture(0)


def main():
    args = configureArguments()
    configureLogging(args.log)

    logging.info("Starting face collector")
    windowTitle = "Face collector App"

    cv2.namedWindow(windowTitle)

    capture = buildVideoCapture(args.fileName)
    if not capture.isOpened():
        logging.error("Arrgghhh! The camera is not working!")
        return

    faceCascade = loadCascadeClassifier(args.haarFolder + "/haarcascade_frontalface_alt2.xml")
    leftEyeCascade = loadCascadeClassifier(args.haarFolder + "/haarcascade_lefteye_2splits.xml")
    rightEyeCascade = loadCascadeClassifier(args.haarFolder + "/haarcascade_righteye_2splits.xml")

    rectColor = (120, 120, 120)
    rectThickness = 2
    formatParams = [int(cv2.IMWRITE_JPEG_QUALITY), 100]

    width = capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
    logging.debug("Image width {0}".format(width))
    minFaceSize = (int(width * 0.25), int(width * 0.25))
    minEyeSize = (12, 18)

    contador = 1
    frameCount = 0

    logging.debug("Reading capture...")
    readOk, image = capture.read()
    outputSize = calculateScaledSize(args.outputWidth, image=image)

    sshClient = MultipleSSHClient(args.sshHost, args.sshUser, args.sshPassword)

    try:
        while (cv2.waitKey(5) == -1 and readOk):

            # Skip 2 of 3 frames
            if frameCount % 20 == 0:

                faces = detectFaces(image, faceCascade, leftEyeCascade, rightEyeCascade, minFaceSize, minEyeSize)

                for (x, y, w, h, leftEye, rightEye) in faces:
                    lCenter = calculateCenter(leftEye[0])
                    rCenter = calculateCenter(rightEye[0])
                    logging.debug("Cropping face. Left eye center {0}. Right eye center {1}".format(lCenter, rCenter))
                    cropped = image[y:y+h, x:x+w]

                    filename = buildFilename(contador)
                    localpath = buildFilepath(filename, args.outFolder)
                    cv2.imwrite(localpath, cropped, formatParams)

                    # sshClient.send(filename, localpath, args.remoteFolder)
                    logging.info('SSH sent [{0}] from [{1}] to [{2}]'.format(filename, localpath, args.remoteFolder))

                    contador += 1

                    drawRectangle(image, (x, y, w, h), rectColor, rectThickness)

            cv2.imshow(windowTitle, cv2.resize(image, outputSize))
            frameCount += 1

            readOk, image = capture.read()

    except KeyboardInterrupt:
        pass
    finally:
        del(capture)
        cv2.destroyWindow(windowTitle)


if __name__ == '__main__':
    main()
